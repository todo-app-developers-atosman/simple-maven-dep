package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep {
    public static void main(String[] args) {
        if (args.length > 0) {
            String name = args[0];
            System.out.println("Hello !");
        } else {
            System.out.println("Hello!");
        }
    }
}
